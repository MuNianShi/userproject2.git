package com.nf.action;

import com.opensymphony.xwork2.ActionSupport;

public class TestAction extends ActionSupport {
    /* 修改与9.21*/
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /* 修改与9.21*/
    @Override
    public String execute() {
        message = "测试一下";
        return this.SUCCESS;
    }
}
